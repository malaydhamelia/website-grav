---
title: Malay Dhamelia

person:
  contact:
    email: malay.n.dhamelia@gmail.com

  portrait:
    url: me_.png
    alt: Malay Dhamelia

download:
  url: resume.pdf
  alt: Resume

taxonomy:
  category:
    - function
  tag:
    - me
---

How do you judge a person? By her/his abilities? By his/her intentions?

===
> "A human being should be able to change a diaper, plan an invasion, butcher a hog, conn a ship, design a building, write a sonnet, balance accounts, build a wall, set a bone, comfort the dying, take orders, give orders, cooperate, act alone, solve equations, analyze a new problem, pitch manure, program a computer, cook a tasty meal, fight efficiently, die gallantly. Specialization is for insects."
> — Robert Heinlein, Time Enough for Love

I am a _believer_ of this quote, trying to be a _follower_ as well. So, I try to keep my information platter as varied as possible, at the same time, not being a _information-hippy_. I majorly live in information hunter-gatherer reality, and try to keep my experiences as diverse as possible. I believe, having varied experiences not only allow us to survive, but have fun in life as well. There is some virtue associated with knowing and doing things diversely. I try to be virtuous in that regard.


### My Interests (combinations of these)
 + Programmable arts (generative arts, algorithmic arts, and so forth) 
 + Creative technologies
 + Tangible interactions
 + Design Research
 + Social psychology
 + Behaviour economics
 + Economics
 + Design methods
 + Contemporary music
 + Impressionism
 + Interpretive Mathematics
 + Philosophy