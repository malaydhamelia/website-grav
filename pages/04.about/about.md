---
title: About

intro:
  enabled: true
  text_enabled: true
  text: I am Malay...

collection_head:
  enabled: true

download:
  url: resume.pdf
  alt: Resume

content:
  items: '@self.children'
  order:
    by: default
    dir: asc
    custom:
      - malay_more_dhamelia

  pagination: false
  url_taxonomy_filters: true
---
I <abbr title="August 11, 2018">recently</abbr> graduated from IDC School of Design, IIT Bombay as Interaction Designer. Just like in my previous <abbr title="B.Tech, Electrical Engineering">education</abbr> I found more things about my interests apart from the design programme. I am more interested in asking questions, and finding answers. Although I came to IDC to solve one question---"Why am I making, what I am making?", I found the joy of finding answers to unanswered questions. After Interaction design course, I found the trade of knowledge fascinating. I do not think this section is going to stay updated as since life is organic, and full of surprises. However, I'll try to keep this website for representation of myself in the best manner as possible.