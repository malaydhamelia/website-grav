---
title: De-convolving Migration Methodology via detailed assessment and Cognitive Learning

download:
	url: poster.pdf
	alt: Published Poster

---

Bhat G, Chandra A, Dhamelia M, Teketi R, Kadam D. (2016).  De-convolving Migration Methodology via detailed assessment and Cognitive Learning, IEEE International Conference on Cloud Computing for Emerging Markets, Poster

===

## Abstract
The application migration process from mainframe to cloud environment turns out to be quite complicated: error prone, time consuming, and costly. Even worse, the application may not work correctly after the sophisticated migration process. Existing approaches mainly complete this process in an ad-hoc manual manner and thus the chances of error are very high. Thus how to migrate the applications to the cloud platform correctly and effectively poseds a critical challenge for the IT service industry as well as clients. This paper dissects the migration process into various touch points to be addressed while deciding for the migration and addresses it using an adaptive assessment to generate scores for various possible solution approaches to the same; which are then used to decide upon an approach for the workload
