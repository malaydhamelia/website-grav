---
title: Pulse:​ ​Opportunistic​ ​And Context-Driven​ ​Crowd​ ​Sensing​ ​for Facilitating​ ​e-Governance​ ​in​ ​India

download:
	url: pulse.pdf
	alt: Paper
---

Bhuyan T, Dhamelia M, Kamboj V. (2017). Pulse:​ ​Opportunistic​ ​And Context-Driven​ ​Crowd​ ​Sensing​ ​for Facilitating​ ​e-Governance​ ​in​ ​India, Submitted to IndiaHCI 2018

===

## Abstract
The advent of smart cities has increased the demand for agile and effective governance. This paper presents a prototype at the proof of concept stage, called “Pulse”. The system comprises of - a mobile app for citizens to submit feedback, a web dashboard for district administrators and government bodies to push close-ended questions based on locality, gender, age group and time, and a network of eddystone beacons that pushes close-ended questions about urban spaces, creating a hyperlocal crowdsensing framework in the Indian context. The collected data can be later analysed to generate meaningful insights and visualizations for better governance and smart decision/policy making.