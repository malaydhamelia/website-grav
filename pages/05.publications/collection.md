---
title: Publications

intro:
  enabled: true
  text_enabled: true
  text: Publications

collection_head:
  enabled: true
  list_items: true

content:
  items: '@self.children'
  order:
    by: title
    dir: asc
    custom:
      - post1
      - post2

  pagination: false
  url_taxonomy_filters: true
---
Some of my projects involve research about the domain, or research about the methodologies/approaches. I publish in various journals as writing allows me to think more, and to get critical feedback about my research. 
<!-- On Writing -->