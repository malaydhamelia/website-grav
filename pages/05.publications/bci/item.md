---
title: Load and Audio Visual Control using Brain Computer Interface

download:
  url: bci_paper.pdf
  alt: Published Poster
---

Vora M, Dhamelia M. (2014). Load and Audio Visual Control using Brain Computer Interface. International Conference on Multidisciplinary Research & Practice (2), 160-165.

===
## Abstract
Load and AV Control using BCI is an implementation of BCI to give a full fledged control to the physically challenged and the paralysed. We further discuss and present an application of BCI on AV, telecommunication, e-mailing, load control using IR and Z-wave protocols using integrated development environment. Also, the scope of using BCI as a control parameter is explored and implemented.

