---
title: Websites with list of conferences and active CFPs
intro:
  enabled: true

taxonomy:
  category:
    - blog
  tag:
    - list
    - resource
    - academic
  author:
    - Malay
---
+ [WikiCFP](http://www.wikicfp.com/cfp/call?conference=HCI)
+ [Core](http://portal.core.edu.au/conf-ranks/)