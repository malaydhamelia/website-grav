---
title: Podcasts
intro:
  enabled: true

taxonomy:
  category:
    - blog
  tag:
    - list
    - resource
  author:
    - Malay
date: 1 October, 2017
---

Podcast Series 

+ 21st Century Myths – BBC Radio
+ BBC World Service
+ Ted Radio Hour
+ Planet Money (Thanks to [Astrid Bin](http://www.astridbin.com/))
+ Freakonomics
