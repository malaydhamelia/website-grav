---
title: Sanskar
intro:
  enabled: true

taxonomy:
  category:
    - blog
  tag:
    - quote

publish_date: 08/29/2018 10:37 pm
---
#### Kab Hoga ?

Hoga!
Hoga!


Kaise Hoga ?

Hoga! 
Hoga! (29/8/2018)

#### Pain___Sadness (<, >, =)

Pain is better than sadness. Pain teaches you. It makes you act. Sadness gives you idleness. (27/8/2018)

#### Hope

Larger and bigger goals give you hope. Sometimes they are necessary for standing up. They are crucial for planning. Shorter goals make you act immediately. (16/6/2018)

#### Perverts

To say that designers are wackos and weirdos is a perverted way of seeing them. It makes you a pervert. (28/8/18)

