---
title: Interesting people of the web
intro:
  enabled: true

taxonomy:
  category:
    - blog
  tag:
    - list
    - resource
  author:
    - Malay

date: 8/3/2018, 4:04 pm

published: false
---

NOT people with interesting websites (because, websites are conduits) or interesting people with websites (well, it would be worth knowing such people yourself. It is fate dictated.)

===
+ [Moiz Syed](http://moiz.ca/) is a designer and a coder. He deals with information design, data journalism, and designs for activism. 
