---
title: Favourite wikis

intro:
  enabled: true

taxonomy:
  category:
    - blog
  tag:
    - favourites
    - resource
    - wiki
    - list
  author:
    - Malay

date: 03/07/2017
---

A list containing interesting [wiki](https://en.wikipedia.org/wiki/Wiki)s. 

===

## Favourite wikis
So, for the recently found out love of wikis of different domains and on different subjects, I will update them in this page

+ [ErgoPedia](http://www.ergopedia.ca/)
+ [Designwiki](http://deseng.ryerson.ca/dokuwiki/start)
+ [WikiIndex](https://wikiindex.org/Welcome)-a wiki about wikis 
