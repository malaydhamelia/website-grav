---
title: Work culture be like 30 Rock
intro:
  enabled: true

taxonomy:
  category:
    - blog
  tag:
    - favourites
    - list
    - TV(Humor)
    - Entertainment
  author:
    - Malay

date: 6 September, 2017

published: false
---

|Season/Episode | Episode name | Comments |
|---------------|--------------|----------|
|1/10|	The Rural Juror| |
|1/11|	The head and the hair| |
|1/12|	Black tie| |
|1/13|	The C word|[Where is my runt ?]|
|2/3|	The collection|	The encounter of Jack and his private investigator|
|4/9|	Klaus and Greta| Imma add one girl to my entourage|
|4/13|	Anna Howard Show day| Liz Lemon getting tripsy|


Also, I think [Rachel Dratch](https://en.wikipedia.org/wiki/Rachel_Dratch) is as cool as Tina