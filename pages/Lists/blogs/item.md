---
title: Blog list
intro:
  enabled: true

taxonomy:
  category:
    - blog
  tag:
    - list
    - resource
  author:
    - Malay
date: 1 October, 2017
---

Blogs

+ [Hatnote](http://blog.hatnote.com/) | A blog dedicated to wiki life. A perennial source of information about WikiMediaFoundation. A very cool repository of wiki realised projects.
+ [Hyperbole and a half](http://hyperboleandahalf.blogspot.in/)
+ [Gates Notes](https://www.gatesnotes.com/)


