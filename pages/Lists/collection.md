---
title: Lists

intro:
  enabled: true
  text_enabled: true
  text: Lists

collection_head:
  enabled: true
  list_items: true

content:
  items: '@self.children'
  order:
    by: title
    dir: asc
    custom:
      - post1
      - post2

  pagination: false
  url_taxonomy_filters: true
---
Lists of various categories.
<!-- On Writing -->