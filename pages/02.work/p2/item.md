---
title: Master's design project 2
---
####Interventions to change handwashing behaviour
A theory driven behavioural intervention to improve hand washing behaviour in households of urban slums.

===

## Abstract
Hands come in contact with a variety of surfaces and fluids. This makes hands, a primary vector of many communicable diseases. Acute diarrhoeal diseases, acute respiratory diseases can be prevented by a simple habit of washing hands at appropriate times. This habit is very essential for households of urban slums and rural areas since the surrounding is an ideal hotbed for pathogens. The effect of hand washing is acknowledged by medical experts and the world health organisation (WHO). We propose a design based interventions suitable for such households that employ behaviour design principles. The interactions piggy back on a household’s existing habits to modify their hand washing behaviour. The interventions are frugal devices that can be deployed at scale and homogenous to their environment. We evaluate the interventions for hand washing behaviour using qualitative and quantitative methods to prove their effectiveness.

## Approach

1. Understanding existing hand hygiene frameworks to study the hand washing activity
2. Field study: Participant observation + Contextual inquiry
3. Analysis using behavioural models
4. Narrowing down to significant parameters using qualitative methods
5. Design explorations
+ Deciding the criteria by pairwise comparison
+ Using WDM to decide for the final intervention
6. Field testing
+ Pilot	
+ Evaluation protocol
+ Evaluation
+ Analysis
