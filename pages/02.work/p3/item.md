---
title: Master's design project 3


download:
	url: p3_report.pdf
	alt: Report
	
---
####Interventions for awareness of noise pollution
Noise is defined as unwanted sound. The sound which pleases listeners is music and that which auses pain and annoyance is noise. At times what is music for some is noise for others. This difference between sound and noise is clearly visible when a group openly celebrates without considering its effects on those nearby. The sufferers go through the physical and psychological consequences, at times, without complaining. This shows a lack of pro-environmental behaviour among the community. We propose a behaviour design led intervention that attempts to embed pro-environmental behaviour in public.

===

## Abstract
Sonic environment consisting of various sounds ranging from music and natural sounds to artificial sounds is an integral part of any geography. It is a reflection of people living in the location as well as an influencer of culture in that location. To assess the perception of a person or people of context offered by that location, a soundscape assessment is performed that majorly involve likert based questions for pleasantness, annoyance, appropriateness and so forth. 

We propose a qualitative method to assess soundscapes from a cultural point of view derived and tweaked from existing methods and tools like the Swedish soundscape quality protocol(SSQP). The method stands as a counterpart of qualitative soundscape assessment approaches like the SSQP, however, it provides richer insights into the perception of the soundscape with its effects, sound characteristics and cultural meanings. 

The developed method is also deployed to a field study in Phulenagar, an urban slum of Mumbai. Regions such as these, occupied by people of low socio-economic stratum have a rich sonic interactions because of small residences and proximity of residences. They also offer rich soundscape which is created and affected by sounds generated from various cultural, social, and economic sources and activities. The analysed results are presented in the form of insights. 