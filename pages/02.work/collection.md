---
title: Work

intro:
  enabled: true
  text_enabled: true
  text: Academic Projects

collection_head:
  enabled: true
  list_items: true

content:
  items: '@self.children'
  order:
    by: title
    dir: asc
    custom:
      - post1
      - post2

  pagination: false
  url_taxonomy_filters: true
---
Projects undertaken during my stay @IDC, IIT Bombay. 
