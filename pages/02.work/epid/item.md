---
title: Epidemic Informatics: Real time visualisation of epidemics	

download:
  url: epidInfo.pdf
  alt: Download presentation
---


A system for health department of BMC to generate real time information visualisation of epidemic spread.

===

## Overview
Preventing epidemic from occurring and spreading is the one of the major motives of any government health ward. Collecting information about the epidemics is done via manual reporting in the current state. Information about epidemics require faster ways of reporting to take actions and prevent it from spreading. We leveraged the increased usage of smartphone to increase a faster reporting of epidemics in a city.

We conducted user studies to understand the existing flow of information between entities ranging from the epidemic cell to local foot soldiers over a period of two weeks to design and develop an application that will allow faster reporting with very less cognitive load on the foot soldiers.

