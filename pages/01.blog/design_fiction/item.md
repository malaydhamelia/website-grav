---
title: Designing artefacts for fiction 

intro:
  enabled: true

taxonomy:
  category:
    - blog
  tag:
    - howTo
    - reflections
    - fiction
  author:
    - malay

publish_date: 4/9/2017 1:00 pm
---

This post will describe the process behind creation of the artefact for design fiction. This post also elaborates on my interpretations and opinions about the course _Trends in Interactive Technologies_ by Prof. Venkat Rajamanickam. 

===

This post in under draft. Please visit the [artefact](https://github.com/femoris/Liron-Liron/wiki/Main-Page) meanwhile.