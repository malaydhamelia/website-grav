---
title: Pre-test/post-test of my thoughts: intervention: IDC
intro:
    enabled: true
    fullscreen: false
    text_enabled: false

taxonomy:
    category:
        - blog
    tag:
        - realisations

publish_date: 08/23/2017 05:50 pm

published: false

---

This post documents experiences, effects, and influences of design education, of IDC on me. A bittersweet symphony as it was, some myths about the design education are debunked, some hypothesis are formed, some ideas are generated. It quotes some people at IDC, and how they influenced me in my thought processes. This post includes a pre-test/post-test analysis of my thoughts. 

===

#### A prequel
I entered IDC as a rider of randomness, unstructured in thoughts. I also believed strongly that to bring structure to your thoughts, one has to be critical in examination about events, actions, the world and self. I imagined myself on a quest to satisfy curiosity about self. To be able to answer the whys of my decisions, my actions. Some say, this issue was in the domain of philosophy, I thought of it as a design problem. Designing self. My goal to understand design, as clear as it could get was to understand the reasons of my actions towards creation; why am I making what I am making ? To objectively understand why are products made, what are the forces that decide why a product is made.

#### 