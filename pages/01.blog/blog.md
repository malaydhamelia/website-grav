---
title: Blog

intro:
  enabled: true
  text_enabled: true
  text: Blog

collection_head:
  enabled: true
  list_items: false

content:
  items: '@self.children.children'
  limit: 5
  order:
    by: publish_date
    dir: desc
  pagination: true
  url_taxonomy_filters: true
---

##[_Archive_](/archives)


##[_Resources, Lists_](/Lists)