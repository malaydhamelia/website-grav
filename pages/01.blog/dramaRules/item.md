---
title: Rules de drama
intro:
    enabled: true
    fullscreen: false
    text_enabled: false

taxonomy:
    category:
        - blog
    tag:
        - realisations
        - Fundae

publish_date: 08/23/2017 05:50 pm
---

![](capture-2.gif)

===
1. Everyone is a participant of a drama. 
1. Everyone plays. They play well.

What is playing well ? Who defines the rules ?

1. Everyone plays well, and the drama goes smooth if everyone played their part. 
Who sets the roles ?
1. Everything plays fine until you play by their rules. 
What if you broke their rules ?
