---
title: Play

intro:
  enabled: true
  text_enabled: true
  text: Play. Fail. Learn

collection_head:
  enabled: true
  list_items: true

content:
  items: '@self.children'
  order:
    by: title
    dir: asc
    custom:
      - post1
      - post2

  pagination: false
  url_taxonomy_filters: true
---
This is a collection of self initiated projects motivated by coolness co-efficients, made with fun, and intended for quirky learning.  
