---
title: Oizom: Sensing air quality for smart cities
---

Air quality meter for smart cities and homes. Developed under Oizom technologies

===
### Making and prototyping 
Prototyped the ideated concept of transmitting Air Quality Data to an online Database. The Database is synced with the App to feed real time Air Quality Checks of the places you want to.

![](oizom_poc.jpg)

The Sensors were assembled as a prototyped systems using Arduino and a GSM shield. The sensors were
calibrated to give the accurate data.



This was designed when Cosire Innovation hired me for their upcoming project called Oizom air quality meters.