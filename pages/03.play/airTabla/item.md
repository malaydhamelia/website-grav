---
title: AirTabla: A theremin inspired tabla
---

Mentoring students of standard 7-10th during an intel sponsored hackathon.

===
### About
As a part of Intel Ideathon, this idea was implemented by 4 kids of age 16-18 years. Inspired from the theramin, using Intel Galileo, kids programmed generation of sound of different frequencies mapped to hand movement. This was taken further and displayed the system at NASSCOM – 2015 and permanent artifact at Tech Museum @ Rashtrapati Bhavan, Delhi
![](schematic.jpg)
![](president_with_my_work.jpg)